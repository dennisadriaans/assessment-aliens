/**
 * A draggable component in Vue.js
 *
 * @@Author  Dennis Adriaansen
 * @param  Object object
 */

Vue.component('draggable', {
    props: ['object'],
    template:
    '<span class="wrap-div" :id="object.id">' +
        '<drag-item :object-item="object"></drag-item>' +
    '<span>',
    components: {
        'drag-item': {
            props:          ['objectItem'],
            template:       '<div :id="objectItem.id" ' +
                                'class="drag-item" ' +
                                'draggable="true" ' +
                                'v-on:drag="drag" ' +
                                'v-on:drop="drop" ' +
                                'v-on:dragover="onDragOver">' +
                                '{{objectItem.menu_title}}' +
                            '</div>',
            methods: {
                onDragOver: function(ev) {
                    ev.preventDefault();
                },
                drag: function(ev) {
                    dragObject = ev.target;
                },
                drop: function(ev) {
                    var target = ev.target;

                    if (! this.targetIsParent(dragObject, target)) {
                        this.scanForObjects(dragObject, target, app.objects);
                        this.swapObjects(firstItem, secondItem);
                        this.saveNewArray();
                    } else {
                        alert('Can\'t override parent node.');
                    }
                },
                targetIsParent: function (dragObject, target) {
                    console.log(target);
                },
                scanForObjects: function(objOne, objTwo, targetArray) {
                    /* TODO learn recursive functions in Vue.js */
                    scan(objOne, objTwo, targetArray);
                },
                swapObjects: function(objOne, objTwo) {
                    var temp = {};
                    for (var prop in objOne) {
                        if (objOne.hasOwnProperty(prop) && objTwo.hasOwnProperty(prop)) {
                            temp[prop] = objOne[prop];
                            objOne[prop] = objTwo[prop];
                            objTwo[prop] = temp[prop];
                        }
                    }
                },
                saveNewArray: function() {
                    var newArray = JSON.stringify(app.objects);
                    localStorage.setItem("navOrder", newArray);
                }
            }
        }
    }
});

var app = new Vue({
    el: '#app',
    data: {
        objects: JSONnav,
        jsonVisible: false,
    },
    methods: {
        reset: function() {
            localStorage.removeItem('navOrder');
            location.reload();
        },
        showJson: function() {
            if (this.jsonVisible) {
                this.jsonVisible = false;
            } else {
                this.jsonVisible = true;
            }
            /* Harder to read but possible:
                this.jsonVisible ? this.jsonVisible = false : this.jsonVisible = true;
            */
        }
    }
});