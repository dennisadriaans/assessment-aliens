if (localStorage.getItem("navOrder") !== null) {
    JSONnav = JSON.parse(localStorage.getItem("navOrder"));
} else {
    JSONnav = [
        {
            id: 0,
            menu_title: 'Home',
            sub_items: []
        },
        {
            id: 1,
            menu_title: 'Over Dakvisie',
            sub_items: [
                {
                    id: 45,
                    menu_title: 'Visie',
                    sub_items: [
                        {
                            id: 53,
                            menu_title: 'Mersdfsdfken',
                            sub_items: []
                        },
                        {
                            id: 54,
                            menu_title: 'Organsdfsdfisaties',
                            sub_items: []
                        }
                    ]
                },
                {
                    id: 46,
                    menu_title: 'Filosofie',
                    sub_items: [
                        {
                            id: 21,
                            menu_title: 'Merken',
                            sub_items: []
                        },
                        {
                            id: 22,
                            menu_title: 'Organisaties',
                            sub_items: [
                                {
                                    id: 87,
                                    menu_title: 'Organisatie',
                                    sub_items: []
                                },
                                {
                                    id: 82,
                                    menu_title: 'Contact',
                                    sub_items: [
                                        {
                                            id: 99,
                                            menu_title: 'Telefoon',
                                            sub_items: []
                                        },
                                        {
                                            id: 95,
                                            menu_title: 'E-mail',
                                            sub_items: []
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            id: 2,
            menu_title: 'Dakonderhoud',
            sub_items: [
                {
                    id: 27,
                    menu_title: 'Dakrenovatie',
                    sub_items: []
                }
            ],
        }
    ]
}
